.. Plom documentation
   Copyright 2022 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later


Preparing an Exam
=================

.. note::

   Stub: move and/or write documentation.


Technical docs
--------------

* The command-line tool :doc:`plom-create` is the current front-end for
  most tasks related starting a new test.

* For scripting or other advanced usage, you can ``import plom.create``
  in your own Python code.  See :doc:`module-plom-create`.
