# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (C) 2022 Edith Coates

from .server_info import (
    GetSpecification,
    ServerVersion,
)

from .identify import (
    GetClasslist,
    GetIDPredictions,
)

from .mark import QuestionMaxMark
